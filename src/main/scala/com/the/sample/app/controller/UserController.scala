package com.the.sample.app.controller

import com.the.sample.app.model.User
import com.the.sample.app.service.UserService
import org.springframework.graphql.data.method.annotation.{Argument, MutationMapping, QueryMapping}
import org.springframework.stereotype.Controller

import java.util
import java.util.Optional


@Controller
class UserController(val userService: UserService) {

  @QueryMapping def findById(@Argument id: Long): Optional[User] = userService.findById(id)

  @QueryMapping def findByEmail(@Argument email: String): Optional[User] = userService.findByEmail(email)

  @QueryMapping def findAll(@Argument page: Int, @Argument pageSize: Int): util.List[User] = userService.findAll(page, pageSize)

  @MutationMapping def createUser(@Argument fullName: String, @Argument email: String): User = {
    val user = new User(fullName = fullName, email = email)
    userService.save(user)
    user
  }

  @MutationMapping def updateUser(@Argument id: Long, @Argument fullName: String, @Argument email: String): User = {
    val user = new User(fullName = fullName, email = email)
    user.id = id
    userService.save(user)
    user
  }

  @MutationMapping def deleteUser(@Argument id: Long): Boolean = {
    try userService.deleteById(id)
    catch {
      case ex: Exception =>
        return false
    }
    true
  }
}
